#!/bin/bash

PROJECT_NAME=`grep "Project name:" detect_logs.log | sed 's/^.*: //'`
PROJECT_VERSION=`grep "Project version:" detect_logs.log | sed 's/^.*: //'`

url_escape() {
  echo `echo $1 | sed s:{:%7B:g | sed s:}:%7D:g`
}

REPORT_URL=`url_escape "https://api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER_UUID/$BITBUCKET_REPO_UUID/commit/$BITBUCKET_COMMIT/reports/1"`
ANNOTATIONS_URL=`url_escape "https://api.bitbucket.org/2.0/repositories/$BITBUCKET_REPO_OWNER_UUID/$BITBUCKET_REPO_UUID/commit/$BITBUCKET_COMMIT/reports/1/annotations/1"`

curl -X DELETE -u synopsys-sig:$APP_PASSWORD $REPORT_URL
curl -X DELETE -u synopsys-sig:$APP_PASSWORD $ANNOTATIONS_URL

echo $BITBUCKET_REPO_OWNER_UUID
echo $BITBUCKET_REPO_UUID
echo $BITBUCKET_COMMIT

java -jar build/libs/bitbucket-sca-reporting-all-1.0-SNAPSHOT.jar \
$BITBUCKET_REPO_OWNER_UUID \
$BITBUCKET_REPO_UUID \
$BITBUCKET_COMMIT \
$BITBUCKET_REPO_OWNER \
$APP_PASSWORD \
$BLACKDUCK_SERVER_URL \
$BLACKDUCK_ACCESS_TOKEN \
$PROJECT_NAME \
$PROJECT_VERSION

# https://api.bitbucket.org/2.0/repositories/{workspace}/{repo_slug}/commit/{node}/reports/{external_id}

echo "\n Finished Uploading!"
