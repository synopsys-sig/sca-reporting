# README #

This project works with gradle, Kotlin & blackduck-commons library

### What is this repository for? ###

* To post security findings of a project into Bitbucket Code Insights
* Version 0.1.0

### How do I get set up? ###

* Download Gradle
* Install Java (JDK 8)
* Install Kotline 1.8

### Contribution guidelines ###

* Writing tests
* Open a Pull request for Code Review


### How to test this repo? ###

* Execute ```gradle fatJar``` to generate jar file.
* The required parameters to run the Jar file
  - repoOwnerUuid (we get this from Bitbucket through pipes)
  - repoUuid (we get this from Bitbucket through pipes)
  - commitId (we get this from Bitbucket through pipes)
  - repoOwner (we get this from Bitbucket through pipes)
  - appPassword (created through the Bitbucket UI, Go to profile & settings -> settings -> App Password -> generate)
  - blackDuckURL (The URL of the Black Duck instance)
  - blackDuckToken (The token generated from Black Duck)
  - projectName (The name of the project being reported)
  - projectVersion (The name of the project version being reported)


### Who do I talk to? ###

* Repo owner or admin
* Email partner-solutions@synopsys.com