package com.synopsys.prototype

import com.google.gson.GsonBuilder
import com.squareup.okhttp.*
import com.synopsys.integration.blackduck.api.generated.enumeration.PolicyStatusType
import com.synopsys.integration.blackduck.api.generated.enumeration.VulnerabilityCvss3SeverityType
import com.synopsys.integration.blackduck.api.generated.view.ProjectVersionView.COMPONENTS_LINK
import com.synopsys.integration.blackduck.api.generated.view.ProjectVersionView.VULNERABLE_COMPONENTS_LINK
import com.synopsys.integration.blackduck.configuration.BlackDuckServerConfig
import com.synopsys.integration.blackduck.configuration.BlackDuckServerConfigBuilder
import com.synopsys.integration.blackduck.rest.ApiTokenBlackDuckHttpClient
import com.synopsys.integration.blackduck.service.BlackDuckServicesFactory
import com.synopsys.integration.blackduck.service.ComponentService
import com.synopsys.integration.blackduck.service.ProjectBomService
import com.synopsys.integration.blackduck.service.ProjectService
import com.synopsys.integration.blackduck.service.model.ProjectSyncModel
import com.synopsys.integration.blackduck.service.model.ProjectVersionWrapper
import com.synopsys.integration.exception.IntegrationException
import com.synopsys.integration.log.IntLogger
import com.synopsys.integration.log.LogLevel
import com.synopsys.integration.log.PrintStreamIntLogger
import com.synopsys.integration.util.IntEnvironmentVariables
import com.synopsys.prototype.model.Annotation
import com.synopsys.prototype.model.Datum
import com.synopsys.prototype.model.Report
import com.synopsys.prototype.model.Value
import java.io.File
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

const val externalId = "synopsysBlackDuck-001"

fun main(args: Array<String>){
    // Input
    var repoOwnerUuid = args[0].replace("{","").replace("}", "")
    var repoUuid = args[1].replace("{","").replace("}", "")
    val commitId = args[2]
    val repoOwner = args[3]
    val appPassword = args[4]
    val blackDuckURL = args[5]
    val blackDuckToken = args[6]
    val projectName = args[7]
    val projectVersion = args[8]

    repoOwnerUuid = "%7B$repoOwnerUuid%7D"
    repoUuid = "%7B$repoUuid%7D"
    println("OWNER_UUID: $repoOwnerUuid, UUID: $repoUuid, commit_id: $commitId")

    // Setup
    val blackDuckServicesFactory: BlackDuckServicesFactory =
        createBlackDuckServicesFactory(blackDuckURL,blackDuckToken)
    val projectService = blackDuckServicesFactory.createProjectService()
    val blackDuckService = blackDuckServicesFactory.createBlackDuckService()
    val logger = blackDuckServicesFactory.logger
    val componentService: ComponentService = blackDuckServicesFactory.createComponentService()
    val projectBomService = ProjectBomService(blackDuckService, logger, componentService)

    // component adding method
    val projectVersionWrapper = getOrCreateProject(
        projectService,
        projectName,
        projectVersion
    )

    var status = PolicyStatusType.NOT_IN_VIOLATION
    val policyStatusView = projectBomService.getPolicyStatusForVersion(projectVersionWrapper.projectVersionView)
    policyStatusView.ifPresent { t -> status = t.overallStatus }

    val report = Report()
    report.reportType = "SECURITY"
    report.title = "Synopsys SCA Report"
    report.details = "Manage security, quality, and license compliance risk that comes from the use of open source and third-party code in applications and containers."
    report.reporter = "Black Duck by Synopsys"

    // policy
    if (status == PolicyStatusType.IN_VIOLATION) {
        report.result = "FAILED"
    } else {
        report.result = "PASSED"
    }

    report.link = projectVersionWrapper.projectVersionView.getFirstLink(COMPONENTS_LINK).get()
    report.logoUrl = "https://www.cloudfoundry.org/wp-content/uploads/icon_synopsys@2x.png"

    // data
    val data = mutableListOf<Datum>()
    val datum = Datum()
    datum.title = "Link"
    datum.type = "LINK"

    val value = Value()
    value.href = projectVersionWrapper.projectVersionView.getFirstLink(VULNERABLE_COMPONENTS_LINK).get()
    value.text = "Vulnerable Component List"
    datum.value = value
    data.add(datum)
    report.data = data

    // date
    val tz = TimeZone.getTimeZone("UTC")
    val df: DateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'") // Quoted "Z" to indicate UTC
    df.timeZone = tz
    val nowAsISO = df.format(Date())
    report.createdOn = nowAsISO
    report.updatedOn = nowAsISO

    // printing report
    val gsonPretty = GsonBuilder().setPrettyPrinting().create()
    val reportAsString = gsonPretty.toJson(report)
    val fileName = "blackduck-report.json"
    val file = File(fileName)
    file.writeText(reportAsString)
    println("---------------------------------------------------- REPORT JSON DUMPED ---------------------------------------------------------")

    // posting report
    putReportRequest(reportAsString, repoOwnerUuid, repoUuid, commitId, repoOwner, appPassword)
    println("---------------------------------------------------- ANNOTATION POSTED ---------------------------------------------------------")

    // Annotations
    val annotations = mutableListOf<Annotation>()
    val componentVulnerabilities = projectBomService.getComponentVersionVulnerabilities(projectVersionWrapper.projectVersionView)
    for (componentVulnerability in componentVulnerabilities) {
        for (vulnerability in componentVulnerability.vulnerabilities) {
            val annotation = Annotation()
            annotation.annotationType = "VULNERABILTIY"
            if (vulnerability.severity != VulnerabilityCvss3SeverityType.CRITICAL) {
                annotation.severity = vulnerability.severity.toString()
            } else {
                annotation.severity = VulnerabilityCvss3SeverityType.HIGH.toString()
            }
            annotation.summary = vulnerability.description.take(50)
            annotation.details = vulnerability.description
            annotation.link = vulnerability.meta.href

            annotations.add(annotation)
        }
    }

    // printing annotations
    val annotationsAsString = gsonPretty.toJson(annotations)
    val annotationFileName = "blackduck-annotation.json"
    val annotationFile = File(annotationFileName)
    annotationFile.writeText(annotationsAsString)
    println("---------------------------------------------------- ANNOTATION JSON DUMPED ---------------------------------------------------------")


    // posting annotations
    annotations.forEachIndexed { index, annotation ->
        val annotationAsString = gsonPretty.toJson(annotation)
        putAnnotationRequest(annotationAsString, repoOwnerUuid, repoUuid, commitId, index+1, repoOwner, appPassword)
    }
    println("---------------------------------------------------- ANNOTATION POSTED ---------------------------------------------------------")
}

fun putReportRequest(data: String?, repoOwnerUuid: String, repoUuid: String, commitId: String, repoOwner: String, appPassword: String) {
    val client = OkHttpClient()
    val mediaType: MediaType = MediaType.parse("application/json")
    val body: RequestBody = RequestBody.create(mediaType, data.toString())

    val userpass = "$repoOwner:$appPassword" //"STLMZjS4hfkuXL4xe8MH"
    val basicAuth = "Basic " + userpass.base64encoded
    val url = "https://api.bitbucket.org/2.0/repositories/$repoOwnerUuid/$repoUuid/commit/$commitId/reports/$externalId"
    println("URL: $url")
    val request: Request = Request.Builder()
        .url(url)
        .put(body)
        .addHeader("Content-Type", "application/json")
        .addHeader("Authorization", basicAuth)
        .build()

    val response: Response = client.newCall(request).execute()
    println("Response ~> code: ${response.code()}, message: ${response.message()}, body: ${response.body().string()}")
}

fun putAnnotationRequest(data: String?, repoOwnerUuid: String, repoUuid: String, commitId: String, count: Int, repoOwner: String, appPassword: String) {
    val client = OkHttpClient()
    val mediaType: MediaType = MediaType.parse("application/json")
    val body: RequestBody = RequestBody.create(mediaType, data.toString())

    val userpass = "$repoOwner:$appPassword"  //"STLMZjS4hfkuXL4xe8MH"
    val basicAuth = "Basic " + userpass.base64encoded
    val url = "https://api.bitbucket.org/2.0/repositories/$repoOwnerUuid/$repoUuid/commit/$commitId/reports/$externalId/annotations/$count"
    // println("URL: $url")
    val request: Request = Request.Builder()
        .url(url)
        .put(body)
        .addHeader("Content-Type", "application/json")
        .addHeader("Authorization", basicAuth)
        .build()

    val response: Response = client.newCall(request).execute()
    println("Response ~> code: ${response.code()}, message: ${response.message()}, body: ${response.body().string()}")
}

fun createIntLogger(): IntLogger? {
    return PrintStreamIntLogger(System.out, LogLevel.INFO)
}

@Throws(IllegalArgumentException::class, IntegrationException::class)
fun createBlackDuckServicesFactory(url: String, token: String): BlackDuckServicesFactory {
    return createBlackDuckServicesFactory(createIntLogger(), url, token)
}

@Throws(java.lang.IllegalArgumentException::class, IntegrationException::class)
fun createBlackDuckServicesFactory(logger: IntLogger?, url: String, token: String): BlackDuckServicesFactory {
    val blackDuckServerConfig: BlackDuckServerConfig =
        getBlackDuckServerConfig(url, token)
    return createBlackDuckServicesFactory(blackDuckServerConfig, logger)
}

@Throws(java.lang.IllegalArgumentException::class, IntegrationException::class)
fun createBlackDuckServicesFactory(blackDuckServerConfig: BlackDuckServerConfig, logger: IntLogger?): BlackDuckServicesFactory {
    val blackDuckHttpClient: ApiTokenBlackDuckHttpClient = blackDuckServerConfig.createApiTokenBlackDuckHttpClient(logger)
    val intEnvironmentVariables = IntEnvironmentVariables()
    val gson = BlackDuckServicesFactory.createDefaultGson()
    val objectMapper = BlackDuckServicesFactory.createDefaultObjectMapper()
    return BlackDuckServicesFactory(intEnvironmentVariables, gson, objectMapper, null, blackDuckHttpClient, logger)
}

fun getBlackDuckServerConfig(url: String , token: String ): BlackDuckServerConfig {
    val builder = BlackDuckServerConfigBuilder()
    builder.url = url
    builder.apiToken = token
    builder.isTrustCert = true
    return builder.build()
}

@Throws(java.lang.IllegalArgumentException::class, IntegrationException::class)
fun getOrCreateProject(projectService: ProjectService, projectName: String, projectVersionName: String): ProjectVersionWrapper {
    val projectView = projectService.getProjectByName(projectName)
    val projectSyncModel = ProjectSyncModel.createWithDefaults(projectName, projectVersionName)
    return if (projectView.isPresent) {
        val projectVersionView = projectService.getProjectVersion(projectView.get(), projectVersionName)
        if (projectVersionView.isPresent) {
            val msg = "Getting project, name ~> $projectName | version ~> $projectVersionName"
            println(msg)
            ProjectVersionWrapper(projectView.get(), projectVersionView.get())
        } else {
            projectService.createProjectVersion(projectView.get(), projectSyncModel.createProjectVersionRequest())
            val msg = "Creating project version, name ~> $projectName | version ~> $projectVersionName"
            println(msg)
            projectService.getProjectVersion(projectName, projectVersionName).get()
        }
    } else {
        projectService.createProject(projectSyncModel.createProjectRequest())
        val msg = "Creating project, name ~> $projectName | version ~> $projectVersionName"
        println(msg)
        projectService.getProjectVersion(projectName, projectVersionName).get()
    }
}


/**
 * Base64 encode a string.
 */
private const val BASE64_SET = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
val String.base64encoded: String get() {
    val pad  = when (this.length % 3) {
        1 -> "=="
        2 -> "="
        else -> ""
    }
    var raw = this
    (1 .. pad.length).forEach { _ -> raw += 0.toChar() }
    return StringBuilder().apply {
        (raw.indices step 3).forEach { it ->
            val n: Int = (0xFF.and(raw[ it ].toInt()) shl 16) +
                    (0xFF.and(raw[it+1].toInt()) shl  8) +
                    0xFF.and(raw[it+2].toInt())
            listOf( (n shr 18) and 0x3F,
                (n shr 12) and 0x3F,
                (n shr  6) and 0x3F,
                n      and 0x3F).forEach { append(BASE64_SET[it]) }
        }
    }   .dropLast(pad.length)
        .toString() + pad
}