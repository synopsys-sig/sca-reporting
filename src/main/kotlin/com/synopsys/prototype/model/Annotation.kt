package com.synopsys.prototype.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Annotation {
    @SerializedName("annotation_type")
    @Expose
    var annotationType: String? = null
    @SerializedName("summary")
    @Expose
    var summary: String? = null
    @SerializedName("details")
    @Expose
    var details: String? = null
    @SerializedName("severity")
    @Expose
    var severity: String? = null
    @SerializedName("link")
    @Expose
    var link: String? = null
}