package com.synopsys.prototype.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Value {
    @SerializedName("text")
    @Expose
    var text: String? = null
    @SerializedName("href")
    @Expose
    var href: String? = null

}